import Network
import UIKit
import Alamofire

public class NetworkLoggerPackage: SessionDelegate {
    
    public init() {}
    
    let storage = NetworkStorage()
    
    // MARK: URLSessionTaskDelegate
    
    public func urlSession(_ session: URLSession, didCreateTask task: URLSessionTask) {
        storage.createTask(task: task)
        print("TEST - ", task.originalRequest?.url)
    }
    
    public override func urlSession(_ session: URLSession, task: URLSessionTask, didFinishCollecting metrics: URLSessionTaskMetrics) {
        super.urlSession(session, task: task, didFinishCollecting: metrics)
    }
    
    public override func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        super.urlSession(session, task: task, didCompleteWithError: error)
    }
    
    public override func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        super.urlSession(session, task: task, didSendBodyData: bytesSent, totalBytesSent: totalBytesSent, totalBytesExpectedToSend: totalBytesExpectedToSend)
    }
    
    // MARK: URLSessionDataDelegate

    public override func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        super.urlSession(session, dataTask: dataTask, didReceive: data)
        storage.updateTask(dataTask: dataTask, didReceive: data)
    }

    // MARK: URLSessionDownloadDelegate

    public override func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        super.urlSession(session, downloadTask: downloadTask, didFinishDownloadingTo: location)
        
    }

    public override func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        super.urlSession(session, downloadTask: downloadTask, didWriteData: bytesWritten, totalBytesWritten: totalBytesWritten, totalBytesExpectedToWrite: totalBytesExpectedToWrite)
    }
}
