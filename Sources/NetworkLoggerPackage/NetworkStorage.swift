//
//  NetworkStorage.swift
//  NetworkLogger
//
//  Created by Сергей Лукичев on 21.06.2023.
//

import Foundation

protocol NetworkStorageDelegate: AnyObject {
    func storageIsUpdated()
}

class NetworkStorage {
    
    weak var delegate: NetworkStorageDelegate?
    
    var tasks: [NetworkTask] = []
    
    func createTask(task: URLSessionTask) {
        
        let guid = task.taskIdentifier
        let url = task.originalRequest?.url?.absoluteString ?? ""
        let httpMethod = task.originalRequest?.httpMethod ?? ""
        let headers = task.originalRequest?.allHTTPHeaderFields
        let body = task.originalRequest?.httpBody
        let request = NetworkTask.Request(createTime: Date(), url: url, httpMethod: httpMethod, headers: headers, body: body)
        
        tasks.append(NetworkTask(guid: guid, request: request, response: nil, status: .pending))
    }
    
    func updateTask(dataTask: URLSessionDataTask, didReceive data: Data) {
        let response = dataTask.response as? HTTPURLResponse
        if let networkTask = tasks.first(where: {$0.guid == dataTask.taskIdentifier}) {
            let response: NetworkTask.Response = .init(createTime: Date(), headers: response?.headers.dictionary, body: data, statusCode: response?.statusCode ?? 0)
            networkTask.response = response
        }
        delegate?.storageIsUpdated()
    }
}

//extension Data {
//    func toDictionary() -> [String:String] {
//        do {
//            if let dictionary = try JSONSerialization.jsonObject(with: self, options: .allowFragments) as? [String:Any] {
//                var result = [String:String]()
//                for (key, value) in dictionary {
//                    if let stringValue = value as? String {
//                        result[key] = stringValue
//                    }
//                }
//                return result
//            }
//        } catch {
//            print("Error converting data to dictionary: \(error)")
//        }
//        return [:]
//    }
//}
