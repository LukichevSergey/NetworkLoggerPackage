//
//  NetworkLoggerPresenter.swift
//  NetworkLogger
//
//  Created by macBook on 21.06.2023.
//  
//

import Foundation

// MARK: Protocol - NetworkLoggerViewToPresenterProtocol (View -> Presenter)
protocol NetworkLoggerViewToPresenterProtocol: AnyObject {
	func viewDidLoad()
}

// MARK: Protocol - NetworkLoggerInteractorToPresenterProtocol (Interactor -> Presenter)
protocol NetworkLoggerInteractorToPresenterProtocol: AnyObject {

}

class NetworkLoggerPresenter {

    // MARK: Properties
    var router: NetworkLoggerPresenterToRouterProtocol!
    var interactor: NetworkLoggerPresenterToInteractorProtocol!
    weak var view: NetworkLoggerPresenterToViewProtocol!
}

// MARK: Extension - NetworkLoggerViewToPresenterProtocol
extension NetworkLoggerPresenter: NetworkLoggerViewToPresenterProtocol {
    func viewDidLoad() {
    
    }
}

// MARK: Extension - NetworkLoggerInteractorToPresenterProtocol
extension NetworkLoggerPresenter: NetworkLoggerInteractorToPresenterProtocol {
    
}