//
//  LoggerTableViewCell.swift
//  logger
//
//  Created by macBook on 20.06.2023.
//

import UIKit
import SnapKit

class LoggerTableViewCell: UITableViewCell {
    static let reuseIdentifier = String(describing: LoggerTableViewCell.self)
 
    private let closeButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        button.setTitleColor(.blue, for: .normal)
        button.setTitle("Close", for: .normal)
        button.backgroundColor = .clear
        button.addTarget(nil, action: #selector(pressCloseButton), for: .touchUpInside)
        return button
    }()
    
    private let shareButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        button.setTitleColor(.blue, for: .normal)
        button.setTitle("S", for: .normal)
        button.backgroundColor = .clear
        button.addTarget(nil, action: #selector(pressCloseButton), for: .touchUpInside)
        return button
    }()
    
    private let menuButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        button.setTitleColor(.blue, for: .normal)
        button.setTitle("M", for: .normal)
        button.backgroundColor = .clear
        button.addTarget(nil, action: #selector(pressCloseButton), for: .touchUpInside)
        return button
    }()
    
    private let url: UILabel = {
        let label = UILabel()
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    
    override init(style: CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    func configuration(withItemModel: NetworkTask) {
        url.text = "\(withItemModel.guid)"
        addSubview(url)
        url.snp.makeConstraints { make in
            make.directionalEdges.equalToSuperview()
        }
        
    }
    
    @objc private func pressCloseButton() {
        print("press button")
    }
}
