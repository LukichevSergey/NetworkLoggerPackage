import XCTest
@testable import NetworkLoggerPackage

final class NetworkLoggerPackageTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(NetworkLoggerPackage().text, "Hello, World!")
    }
}
